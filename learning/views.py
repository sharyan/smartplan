# Create your views here.
from django.template import Context
from django.shortcuts import render
import os


def display_header(request):
    values = request.META.items()
    values.sort()
    context = Context({'headers': values, 'rendered_from': os.path.dirname(__file__).replace('\\', '/')})
    return render(request, 'HttpHeader.html', context)


def hello_name(request):
    error = False
    if 'name' in request.GET:
        if request.GET['name'] and isinstance(request.GET['name'], basestring):
            context = Context({'name': request.GET['name']})
            return render(request, 'Hello.html', context)
        else:
            error = True
    return render(request, 'Base_Form.html', Context({'bad_submit': error}))


def show_base(request):
    return render(request, 'base.html', None)


def team_create(request):
    return render(request, 'team.html', None)