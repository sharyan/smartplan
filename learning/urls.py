__author__ = 'Shane'

from django.conf.urls import patterns, url

from views import show_base, team_create

urlpatterns = patterns('',
                       url(r'^$', show_base),
                       url(r'^team/$', team_create)
)
