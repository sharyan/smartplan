import sys
import os.path
import copy
import unittest
import matplotlib.pyplot as plt
from networkx import nx
from Scheduler.Task import Task, generate_dependency_graph, validate_dependency_graph, TaskManager, get_dependencies
from Scheduler.Developer import Developer


sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

__author__ = 'Shane'

tasks = [Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4"),
         Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5"),
         Task(task_id=3, task_name="Foundation Implementation", task_type="coding", task_duration="10"),
         Task(task_id=4, task_name="Design GUI", task_type="design", task_duration="3"),
         Task(task_id=5, task_name="Design BASE", task_type="design", task_duration="5"),
         Task(task_id=6, task_name="GUI Implementation", task_type="coding", task_duration="8"),
         Task(task_id=7, task_name="Test BASE", task_type="testing", task_duration="10"),
         Task(task_id=8, task_name="Test GUI", task_type="testing", task_duration="6")]

developers = [
    Developer(developer_name="Chris peters", skill_dict={"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Shane Ryan", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Elmo peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Nano peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Helman peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Gorge peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Feder peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Allmon peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10})
]

dependencies = {1: [],
                2: [],
                3: [5],
                4: [1],
                5: [2],
                6: [3, 4],
                7: [3],
                8: [6]}


class DependencyTests(unittest.TestCase):
    def setUp(self):
        self.tasks = copy.copy(tasks)
        self.dependencies = copy.copy(dependencies)

    def test_generate_dependency_graph(self):
        graph = generate_dependency_graph(self.tasks, self.dependencies)
        self.assertTrue(isinstance(graph, nx.DiGraph), "Task returned was not a DiGraph")
        self.assertIsNotNone(graph.edges(), "The dependency graph should have edges showing task dependencies")
        self.assertIsNotNone(graph.nodes(), "The dependency graph should have nodes showing tasks")

    def test_dependency_graph_hierarchy(self):
        graph = generate_dependency_graph(self.tasks, self.dependencies)
        task6 = next((x for x in self.tasks if x.id == 6), None)
        task3 = next((x for x in self.tasks if x.id == 3), None)
        task4 = next((x for x in self.tasks if x.id == 4), None)
        predecessors = graph.predecessors(task6)
        self.assertTrue(task3 in predecessors, "Task 3 should be a predecessor of task 6")
        self.assertTrue(task4 in predecessors, "Task 4 should be a predecessor of task 6")

    @unittest.skip("Skipping to disable showing visualization of testing dependency graph")
    def test_draw_dependency_graph(self):
        graph = generate_dependency_graph(self.tasks, self.dependencies)
        nx.draw(graph)
        plt.draw()
        plt.show()

    def test_dependency_validation(self):
        graph = generate_dependency_graph(self.tasks, self.dependencies)
        self.dependencies[8] = [1, 2]
        self.assertFalse(validate_dependency_graph(self.tasks, self.dependencies, graph=graph))


class TaskManagerTests(unittest.TestCase):
    def setUp(self):
        self.tasks = copy.copy(tasks)
        self.dependencies = copy.copy(dependencies)
        self.assertIsNone(TaskManager.dependency_graph)
        self.manager = TaskManager(self.tasks, self.dependencies)
        self.assertIsNotNone(self.manager,
                             "Manager was not successfully created from the tasks and dependency information provided")

    def tearDown(self):
        TaskManager.dependency_graph = None

    def test_load_dependency_graph(self):
        graph = self.manager.get_dependency_graph()
        self.assertIsNotNone(graph.nodes(data=None))

    def test_remove_task_from_manager(self):
        self.manager.remove_task(self.tasks[7])
        graph = self.manager.get_dependency_graph()
        self.assertFalse(validate_dependency_graph(self.tasks, self.dependencies, graph=graph))

    def test_add_task_to_manager(self):
        self.manager.add_task(Task(task_id=9, task_name="System Tests", task_type="testing", task_duration="4"),
                              {9: [7, 8]})
        graph = self.manager.get_dependency_graph()
        self.assertFalse(validate_dependency_graph(self.tasks, self.dependencies, graph=graph))

    def test_get_bad_dependency(self):
        dependences = get_dependencies(Task(task_id=9, task_name="System Tests", task_type="testing", task_duration="4"))
        self.assertFalse(not dependencies, "Dependencies should be empty for dummy task")


    def test_get_good_dependency(self):
        dependences = get_dependencies(self.tasks[6])
        self.assertTrue(dependencies, "Task should have dependencies associated with it")

def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTests(unittest.TestLoader().loadTestsFromTestCase(DependencyTests))
    test_suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TaskManagerTests))
    return test_suite




