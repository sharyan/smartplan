import random
import unittest
from Scheduler.Developer import Developer, DeveloperManager
from Scheduler.Task import Task, TaskManager
from Scheduler.TimelineModel import Population

__author__ = 'Shane'

NUMBER_SOLUTIONS_IN_POPULATION = 100

tasks = [Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4"),
         Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5"),
         Task(task_id=3, task_name="Foundation Implementation", task_type="coding", task_duration="10"),
         Task(task_id=4, task_name="Design GUI", task_type="design", task_duration="3"),
         Task(task_id=5, task_name="Design BASE", task_type="design", task_duration="5"),
         Task(task_id=6, task_name="GUI Implementation", task_type="coding", task_duration="8"),
         Task(task_id=7, task_name="Test BASE", task_type="testing", task_duration="10"),
         Task(task_id=8, task_name="Test GUI", task_type="testing", task_duration="6")]

developers = [
    Developer(developer_name="Chris peters", skill_dict={"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Shane Ryan", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Elmo peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Nano peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Helman peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Gorge peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Feder peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Allmon peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10})
]

dependencies = {1: [],
                2: [],
                3: [5],
                4: [1],
                5: [2],
                6: [3, 4],
                7: [3],
                8: [6]}


class PopulationTests(unittest.TestCase):

    def setUp(self):
        self.population = Population(NUMBER_SOLUTIONS_IN_POPULATION)
        self.task_manager = TaskManager(tasks, dependencies)
        self.dev_manager = DeveloperManager(developers)
        pass

    def tearDown(self):
        pass

    def test_create_population(self):
        self.population.seed_population(task_manager=self.task_manager, developer_manager=self.dev_manager)
        self.assertEqual(len(self.population.solutions), NUMBER_SOLUTIONS_IN_POPULATION)



def suite():
    suite = unittest.TestLoader().loadTestsFromTestCase(PopulationTests)
    return suite


import random
import bisect
import collections

def cdf(weights):
    total=sum(weights)
    result=[]
    cumsum=0
    for w in weights:
        cumsum+=w
        result.append(cumsum/total)
    return result

def choice(population,weights):
    assert len(population) == len(weights)
    cdf_vals=cdf(weights)
    x=random.random()
    idx=bisect.bisect(cdf_vals,x)
    return population[idx]

weights=[0.1,0.001,0.01,0.9]
population='ABCD'
counts=collections.defaultdict(int)
for i in range(10000):
    counts[choice(population,weights)]+=1
print(counts)



