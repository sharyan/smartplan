import operator
import copy
from Scheduler.Developer import Developer
from Scheduler.Fitness import calculate_dependency_violation, DEPENDENCY_VIOLATION_COST, \
    calculate_duration_violation_cost, calculate_developer_availablilty_cost
from Scheduler.Task import Task, generate_dependency_graph
from Scheduler.TimelineModel import Timeline

__author__ = 'Shane'

import unittest

tasks = [Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4"),
         Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5"),
         Task(task_id=3, task_name="Foundation Implementation", task_type="coding", task_duration="10"),
         Task(task_id=4, task_name="Design GUI", task_type="design", task_duration="3"),
         Task(task_id=5, task_name="Design BASE", task_type="design", task_duration="5"),
         Task(task_id=6, task_name="GUI Implementation", task_type="coding", task_duration="8"),
         Task(task_id=7, task_name="Test BASE", task_type="testing", task_duration="10"),
         Task(task_id=8, task_name="Test GUI", task_type="testing", task_duration="6")]

devs = [
    Developer(developer_name="Chris peters", skill_dict={"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Shane Ryan", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Elmo peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Nano peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Helman peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Gorge peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Feder peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10}),
    Developer("Allmon peters", {"Analysis": 40, "Coding": 50, "Design": 70, "Testing": 10})
]


class TestViolations(unittest.TestCase):
    def setUp(self):
        self.timeline = Timeline()
        self.developers = copy.deepcopy(devs)

    def tearDown(self):
        del self.timeline
        del self.developers

    def test_developer_availability_violation_calculation(self):
        taskA = Task(task_id=1, task_name="Network Analysis", task_type="analysis", task_duration="4")
        taskB = Task(task_id=2, task_name="GUI Analysis", task_type="analysis", task_duration="5")
        taskC = Task(task_id=3, task_name="Base Implementation", task_type="coding", task_duration="10")
        taskD = Task(task_id=4, task_name="Database GUI", task_type="design", task_duration="3")

        # cost = difference in time between slots that is idle
        self.timeline.add_slot(time=1, task=taskD, developer=self.developers[6])  # time = 1 + 3 = 4 from 1 to 4, cost = 0
        self.timeline.add_slot(time=3, task=taskC, developer=self.developers[6])  # time = 3 + 10 + 1 = 13, from 3 - 13, cost = 1
        self.timeline.add_slot(time=10, task=taskB, developer=self.developers[6])  # time = 10 + 5 = 15, from 10 to 15, cost = 3
        self.timeline.add_slot(time=13, task=taskA, developer=self.developers[6])  # time = 13 + 4 = 17, from 13 to 17, cost = 2

        cost = calculate_developer_availablilty_cost(self.timeline)
        self.assertTrue(cost == 6,
                        "Cost for violations should be %i but instead was %i" % (6, cost))

    def test_dependency_violation_calculation_non_multiple_slots(self):
        taskA = Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4")
        taskB = Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5")
        test_tasks = [taskA, taskB]
        dependency_info = {1: [], 2: [1]}
        timeline = Timeline()
        graph = generate_dependency_graph(test_tasks, dependency_info)
        test_tasks = sorted(test_tasks, key=operator.attrgetter('id'))
        for (counter, task) in enumerate(reversed(test_tasks)):
            timeline.add_slot(time=counter * 10, task=task, developer=self.developers[6])
        cost = calculate_dependency_violation(timeline, graph)
        self.assertTrue(cost == DEPENDENCY_VIOLATION_COST,
                        "Cost for a single violation should be %i" % DEPENDENCY_VIOLATION_COST)

    def test_dependency_violation_calculation_multiple_slots(self):
        taskA = Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4")
        taskB = Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5")
        taskC = Task(task_id=3, task_name="Foundation Implementation", task_type="coding", task_duration="10")
        #taskD = Task(task_id=4, task_name="Design GUI", task_type="design", task_duration="3")
        test_tasks = [taskA, taskB, taskC] #, taskD]

        dependency_info = {1: [], 2: [1], 3: [2, 1]} # , 4: [3, 2]}
        graph = generate_dependency_graph(test_tasks, dependency_info)
        test_tasks = sorted(test_tasks, key=operator.attrgetter('id'))

        #timeline.add_slot(time=1, task=taskD, developer=developers[6])  # should cost 6
        self.timeline.add_slot(time=1, task=taskC, developer=self.developers[6])  # should cost 3
        self.timeline.add_slot(time=1, task=taskB, developer=self.developers[6])  # should cost 1
        self.timeline.add_slot(time=3, task=taskA, developer=self.developers[6])  # should cost 0

        cost = calculate_dependency_violation(self.timeline, graph)
        self.assertTrue(cost == 4,
                        "Cost for violations should be %i but instead was %i" % (4, cost))

    def test_duration_violation_calculation_first_test(self):
        taskA = Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4")
        taskB = Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5")
        taskC = Task(task_id=3, task_name="Foundation Implementation", task_type="coding", task_duration="10")

        # cost = difference in time between slots that is idle
        self.timeline.add_slot(time=1, task=taskC, developer=self.developers[6])  # (10 + 1) - 10 = cost of 1
        self.timeline.add_slot(time=10, task=taskB, developer=self.developers[6])  # (10 + 5) - (13 + 4) = 2
        self.timeline.add_slot(time=13, task=taskA, developer=self.developers[6])

        cost = calculate_duration_violation_cost(self.timeline)
        self.assertTrue(cost == 3,
                        "Cost for violations should be %i but instead was %i" % (3, cost))

    def test_duration_violation_calculation_second_test(self):
        taskA = Task(task_id=1, task_name="GUI Analysis", task_type="analysis", task_duration="4")
        taskB = Task(task_id=2, task_name="BASE Analysis", task_type="analysis", task_duration="5")
        taskC = Task(task_id=3, task_name="Foundation Implementation", task_type="coding", task_duration="10")
        taskD = Task(task_id=4, task_name="Design GUI", task_type="design", task_duration="3")


        # cost = difference in time between slots that is idle
        self.timeline.add_slot(time=1, task=taskD, developer=self.developers[2])  # max_time = 1 + 3 = 4, cost = 1
        self.timeline.add_slot(time=3, task=taskC, developer=self.developers[6])  # max_time = 3 + 10 = 13, cost = 3
        self.timeline.add_slot(time=10, task=taskB, developer=self.developers[6])  # max_time = 10 + 5 = 15, cost = 2
        self.timeline.add_slot(time=13, task=taskA, developer=self.developers[6])

        cost = calculate_duration_violation_cost(self.timeline)
        self.assertTrue(cost == 6,
                        "Cost for violations should be %i but instead was %i" % (6, cost))




def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestViolations))
    return test_suite