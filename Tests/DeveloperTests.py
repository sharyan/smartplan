from Scheduler.Developer import Developer

__author__ = 'Shane'
import unittest

# TODO add developer tests

class DeveloperTests(unittest.TestCase):

    def test_add_skill(self):
        developer = Developer(developer_name="Shane Ryan", skill_dict={})
        self.assertFalse(developer.skills,
                         "New instance of developer with empty skill set should have an empty skill set, dumbass")
        skill = {"analysis": 40}
        developer.add_skill("analysis", 40)
        self.assertEqual(developer.skills, skill, "Developer skills %s should equal the added skill %s" %
                                                  (developer.skills, skill))

    def test_add_skills(self):
        developer = Developer(developer_name="Shane Ryan", skill_dict={})
        skills = {"analysis": 40, "testing": 40, "coding": 80, "design": 20}
        developer.add_skills(skills=skills)
        self.assertEqual(skills, developer.skills)



def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTests(unittest.TestLoader().loadTestsFromTestCase(DeveloperTests))
    return test_suite


