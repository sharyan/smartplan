from Tests import ViolationTests, DeveloperTests, PopulationTests

__author__ = 'Shane'
import unittest
import sys
import os.path

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import TaskTests

if __name__ == '__main__':
    task_test_suite = TaskTests.suite()
    violation_test_suite = ViolationTests.suite()
    developer_test_suite = DeveloperTests.suite()
    population_test_suite = PopulationTests.suite()

    unittest.TextTestRunner(verbosity=3).run(task_test_suite)
    unittest.TextTestRunner(verbosity=3).run(violation_test_suite)
    unittest.TextTestRunner(verbosity=3).run(developer_test_suite)
    unittest.TextTestRunner(verbosity=3).run(population_test_suite)