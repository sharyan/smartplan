from django.db import models

# Create your models here.


# TODO rethink the models here for data clarity

# TODO create admin controller classes for the following models if needed


class SkillCategory(models.Model):
    category = models.CharField(max_length=40)


class Skill(models.Model):
    name = models.CharField(max_length=40)
    skill_level = models.IntegerField()
    category = models.ManyToManyField(SkillCategory)


class Developer(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.CharField(max_length=10)
    skills = models.ManyToManyField(Skill)


