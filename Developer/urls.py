__author__ = 'Shane'
from django.conf.urls import patterns, include, url
from views import create_developer, delete_developer, get_profile, manage_developer

urlpatterns = \
    patterns('',
             # TODO implement url parameters for information needed in views
             url(r'^create/$', create_developer),
             url(r'^manage/$', manage_developer),
             url(r'^profile/$', get_profile),
             url(r'^delete/$', delete_developer),

    )


