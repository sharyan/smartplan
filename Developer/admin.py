__author__ = 'Shane'

from django.contrib import admin
from models import Developer, Skill, SkillCategory

# TODO add admin classes for the following models

admin.site.register(Developer)
admin.site.register(Skill)
admin.site.register(SkillCategory)
