from django.db import models

# Create your models here.

class Task(models.Model):
    name = models.CharField(max_length=40)
    description = models.CharField(max_length=1000)
    duration = models.DateTimeField()
    priority = models.IntegerField()

class Dependency(models.Model):
    parent = models.ForeignKey(Task, related_name="parent")
    child = models.ForeignKey(Task, related_name="child")




