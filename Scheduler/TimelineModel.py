import logging
from random import Random
from datetime import datetime
from copy import deepcopy
from Scheduler import Developer, Task
from Scheduler.Developer import DeveloperManager
from Scheduler.Task import TaskManager
from Settings import START_DATE, TIME_UNIT

__author__ = 'Shane'

# create logger
logger = logging.getLogger('root')


class Slot:

    def __init__(self, time, task, developer):
        self.start_time = time
        self.task = task
        self.developer = developer
        self.developer.add_task(task=task, start_time=self.start_time)

    def __str__(self):
        return "Slot: start_time=" + str(self.start_time) + ", " + str(self.task) + ", " + str(self.developer)

    def __repr__(self):
        return self.__str__()


class Timeline:

    def __init__(self):
        self.start_time = START_DATE
        self.quantum = TIME_UNIT
        self.slots = {}
        self.fitness = 0

    def add_slot(self, time=0, task=Task, developer=Developer):
        """
        Sets a task for a time slot
        @type time: integer
        @param time: the numerical time value for the timeline
        @type task: Task
        @param task: the task to fill the time slot
        @return: no return
        """
        new_slot = Slot(time, task, developer)
        if self.slots and time in self.slots.keys():
            self.slots[time].append(new_slot)
        else:
            self.slots[time] = [new_slot]
        pass

    def __str__(self):
        ret = ""
        keys = sorted(self.slots.keys())
        for key in keys:
            ret += "\tSlot: " + str(key) + " value: " + str(self.slots[key]) + "\n"
        return ret

    def __unicode__(self):
        ret = u""
        keys = sorted(self.slots.keys())
        for key in keys:
            ret += u"\tSlot: " + unicode(key) + u" value: " + unicode(self.slots[key]) + u"\n"
        return ret

    def __repr__(self):
        return self.__unicode__()



class Population:

    def __init__(self, number_individuals):
        self.start_date = START_DATE
        self.population_size = number_individuals
        self.solutions = []

    def seed_population(self, task_manager=TaskManager, developer_manager=DeveloperManager):
        for i in range(0, self.population_size):
            self.solutions.append(Timeline())

        rand = Random()
        rand.seed(datetime.now().microsecond)
        tasks = list(task_manager.get_all_tasks())
        developers = list(developer_manager.get_all_developers())
        max_duration = 0
        for task in tasks:
            max_duration += int(task.duration)

        for time_line in self.solutions:
            logger.debug("Working on new seed timeline solution")
            temp_tasks = deepcopy(tasks)
            temp_developers = deepcopy(developers)
            for count, task in enumerate(reversed(temp_tasks)):
                index = rand.randint(0, len(temp_tasks)-1)
                logger.debug("Getting " + str(index) + " from task list of size " + str(len(temp_tasks)))
                task = temp_tasks[index]
                temp_tasks.remove(task)
                developer = temp_developers[count % len(developers)]
                logger.debug("Matching " + str(task) + " to " + str(developer))
                time_line.add_slot(time=rand.randint(0, max_duration), task=task, developer=developer)

    def __str__(self):
        ret = ""
        i = 1
        for timeline in self.solutions:
            ret += "Timeline:  " + str(i) + "\n" + str(timeline) + "\n"
            i += 1
        return ret

    def __repr__(self):
        return self.__str__()


