import logging
import os
from string import split
from xml.etree.ElementTree import ElementTree
from Developer import Developer, DeveloperManager
from Task import Task, TaskManager

__author__ = 'Shane'

CURRENT_PATH = os.path.realpath(os.path.dirname(__file__))
logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__file__)

# Holds all functions and related classes for loading and bootstrapping project data

def load_tasks(project_root):
    """
    Loads the task information from the project data
    @param project_root: the task element tree root of the project xml file
    @return: the list of tasks and the dependency information
    """
    # read the tasks from the project
    task_root = project_root.find('tasks')
    tasks = []
    dependency_information = dict()
    for task in task_root.findall('task'):
        task_id = task.attrib['id']
        for child in task:
            if child.tag == 'name':
                task_name = child.text
            if child.tag == 'duration':
                task_duration = child.text
            if child.tag == 'type':
                task_type = child.text
            if child.tag == 'dependencies':
                task_dependencies = child.text
                if isinstance(task_dependencies, basestring):
                    dependencies_list = split(task_dependencies, ',')
                    task_dependencies = []
                    for dependency_id in dependencies_list:
                        if dependency_id:
                            task_dependencies.append(int(dependency_id))
                    dependency_information[task_id] = task_dependencies
        task = Task(task_id=task_id, task_name=task_name, task_type=task_type, task_duration=task_duration)
        logger.debug("Found task %s" % str(task))
        tasks.append(task)
    return tasks, dependency_information


def load_developers(project_root):
    # read the developers from the project
    developer_root = project_root.find('developers')
    developers = []
    for task in developer_root.findall('developer'):
        # logger.debug("Found developer: %s" % str(task.attrib.get('name')))
        developer_name = task.attrib.get('name')
        developer_skills = {}
        skills_root = task.find('skills')
        for skill in skills_root.findall('skill'):
            developer_skills[skill.attrib['name']] = int(skill.text)
        developer = Developer(developer_name=developer_name, skill_dict=developer_skills)
        developers.append(developer)
        logger.debug("Found %s" % developer)
    return developers


def load_schedule_data(project_root):
    schedule_root = project_root.find('scheduler')
    settings = {}
    for setting in schedule_root:
        if setting.tag == "start":
            settings['start'] = setting.text
        if setting.tag == "timeunit":
            settings['timeunit'] = setting.text
    return settings


def resolve_filename(filename):
    if os.path.isabs(filename) and os.path.exists(filename):
        return filename
    elif split(filename, '.').pop() == "xml":
        return CURRENT_PATH + filename


def load_project(filename):
    """
    Loads the project data from the project xml definition
    :param filename: the file name to load the data from
    :return: the developer and task manager instances
    """
    project_xml = resolve_filename(filename=filename)
    logger.info("Reading from project_xml file @ %s" % project_xml)
    root = ElementTree().parse(project_xml)
    logger.debug("Reading project %s" % str(root.attrib.get('name')))

    tasks, dependency_info = load_tasks(root)
    developers = load_developers(root)
    settings = load_schedule_data(root)

    task_manager = TaskManager(task_list=tasks, dependency_info=dependency_info)
    developer_manager = DeveloperManager(developer_list=developers)

    logger.info("Finished reading project data from " + project_xml)
    return task_manager, developer_manager