import logging
from nose.util import set
from win32com.client.build import NotSupportedException

__author__ = 'Shane'

logger = logging.getLogger("root")


class Developer:

    def __init__(self, developer_name, skill_dict):
        """
        Creates a Developer
        @param developer_name: Name of the developer
        @param skill_dict: Dictionary of skills using the format { str(skill_name): int(skill_level) }
        """
        self.name = developer_name
        self.skills = skill_dict
        self.availability = {}

    def add_skill(self, skill_name, skill_level):
        if isinstance(skill_name, basestring) and isinstance(skill_level, int):
            self.skills.update({skill_name: skill_level})
        else:
            raise Exception('Skill name should be string')

    def add_skills(self, skills={}):
        skill_names = skills.keys()
        for name in skill_names:
            if not isinstance(name, basestring):
                raise Exception('Skill name should be string')
            if not isinstance(skills[name], int):
                raise Exception('Skill value should be an integer')
            self.add_skill(name, skills[name])

    def add_task(self, task, start_time):
        """
        Adds a task to the timeline of the developer. Validates if the task can be added by checking the time period is
        not already taken up, partially or fully, by another task.
        :param task: The task to add to the dev's timeline
        :param start_time:  The start date to add the task to (integer, count in days)
        :return: No return
        """
        end_time = start_time+int(task.duration)
        for i in range(1, len(self.availability)):
            if i not in self.availability:
                self.availability[i] = []
            else:
                break
        for i in range(start_time, end_time):
            if i not in self.availability:
                self.availability[i] = [task, ]
            else:
                self.availability[i].append(task)

    def set_name(self, developer_name):
        self.name = developer_name

    def __repr__(self):
        #return "Developer name =  %s, Developer skills = %s" % (str(self.name), str(self.skills))
        return self.__str__()

    def __str__(self):
        #return "Developer name =  %s, Developer skills = %s" % (str(self.name), str(self.skills))
        return "Developer name =  %s" % (str(self.name))

    def __unicode__(self):
        #return "Developer name =  %s, Developer skills = %s" % (str(self.name), str(self.skills))
        return u"Developer name =  %s" % (unicode(self.name))


class DeveloperManager:

    def __init__(self, developer_list=set()):
        self.developers = developer_list

    def add_developer(self, developer):
        self.developers.add(developer)

    def remove_developer(self, developer):
        if developer in self.developers:
            self.developers.remove(developer)

    def get_all_developers(self):
        return self.developers



