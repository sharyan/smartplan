import logging
import unittest
from nose.util import set
from networkx import nx
from networkx.algorithms import isomorphism
from Scheduler.Exceptions import NotInitiatedError

__author__ = 'Shane'

logger = logging.getLogger("root")



class Task:
    def __init__(self, task_id, task_name, task_type, task_duration):
        self.id = task_id
        self.name = task_name
        self.type = task_type
        self.duration = task_duration

    def __repr__(self):
        return "Task id = %s, Task name = %s" % (str(self.id), self.name)

    def __str__(self):
        return "Task id = %s, Task name = %s" % (str(self.id), self.name)

    def __unicode__(self):
        return u"Task id = %s, Task name = %s" % (
            str(self.id), self.name)

    def __hash__(self):
        return int(self.id)

    def __eq__(self, other):
        return other and self.id == other.id and self.name == other.name \
                   and self.duration == other.duration and self.type == other.type



class TaskManager:

    dependency_graph = None

    def __init__(self, task_list, dependency_info):
        self.tasks = set(task_list)
        self.dependency_information = dependency_info
        TaskManager.dependency_graph = generate_dependency_graph(self.tasks, dependency_info)

    def add_task(self, task, dependency_info={}):
        global dependency_graph
        self.dependency_information = dict(self.dependency_information.items() + dependency_info.items())
        self.tasks.add(task)
        TaskManager.dependency_graph = generate_dependency_graph(self.tasks, self.dependency_information)

    def remove_task(self, task):
        if task in self.tasks:
            self.tasks.remove(task)
            del self.dependency_information[task.id]
            TaskManager.dependency_graph = generate_dependency_graph(self.tasks, self.dependency_information)

    def get_dependency_graph(self):
        if TaskManager.dependency_graph is None:
            TaskManager.dependency_graph = generate_dependency_graph(self.tasks, self.dependency_information)
            return TaskManager.dependency_graph
        else:
            return TaskManager.dependency_graph

    def get_all_tasks(self):
        return self.tasks


def get_dependencies(task=Task):
    """
    Gets all the dependencies of the specified task
    @type Task:
    @param task: the task to get the dependencies of
    @return: a list of all the dependencies
    """
    global dependency_graph
    if TaskManager.dependency_graph.has_node(task) and TaskManager.dependency_graph.neighbors(task):
        return TaskManager.dependency_graph.neighbors(task)


def generate_dependency_graph(tasks=[], dependency_info={}):
    """
    Generates a dependency graph of the tasks with associated dependency information
    @param tasks: The list of tasks
    @param dependency_info: The dictionary of dependency information { task_id : [task_dep_1, task_dep_2, ...]}
    @return: A networkx graph representation of the tasks and dependencies
    """
    graph = nx.DiGraph()
    for task in tasks:
        graph.add_node(task)
        if task.id in dependency_info.keys():
            for dependency in dependency_info[task.id]:
                dependency_task = next((x for x in tasks if int(x.id) == dependency))
                graph.add_node(task)
                graph.add_edge(dependency_task, task)
    return graph


def validate_dependency_graph(tasks, dependency_info, graph):
    """
    Returns true if the the task and dependency information match the dependency graph provided
    @param tasks: The list of tasks
    @param dependency_info: the depenedencies between the tasks
    @param graph: the graph to compare the generated task and dependency data to
    @return: True if graphs are isometrically equal, False if not
    """
    generated_graph = generate_dependency_graph(tasks=tasks, dependency_info=dependency_info)
    return isomorphism.DiGraphMatcher(generated_graph, graph).is_isomorphic()




