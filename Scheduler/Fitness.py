import logging
import unittest
from nose.util import set
from Scheduler import Task
from Scheduler.Task import TaskManager

__author__ = 'Shane'
from networkx import nx
from TimelineModel import Timeline

logger = logging.getLogger("root")

DEPENDENCY_VIOLATION_COST = 1
DEVELOPER_AVAILABILITY_VIOLATION_COST = 1
DURATION_OVERFLOW_COST = 1
# TODO Write developer availability fitness function


def calculate_dependency_violation(timeline=Timeline, dependency_graph=nx.DiGraph):
    """
    Calculates the dependency violation cost for timelines. Uses the dependency precedence graph to check for out of
    place tasks and add a penalty for each predecessor found out of position relative to the compared task.

    pseudocode:

    1. sort the timeline in terms of time slot positions
    2. for each task in timeline
        - precedent_tasks = get_all_precedent_tasks(task)
        - current_task_time_slot = get_time_slot(task, timeline)
        - for each precedent task in precedent tasks
            - task_slot_time = get_task_time(task, timeline)
            - if current_task_time <= task_slot_time
                - add penalty

    @param timeline: the solution in the population to calculate the dependency violation cost of
    @param dependency_graph: The precedence reference
    @return: The cost of the timeline in terms of dependency violations
    """
    times = sorted(timeline.slots.keys())
    cost = 0
    for time in times:
        assignments = timeline.slots[time]
        for assignment in assignments:
            current_task = assignment.task
            current_task_slot = time
            precedent_tasks = get_parent_tasks(current_task, dependency_graph)
            for dependant_task in precedent_tasks:
                dependant_time_slot = get_task_time_slot(task=dependant_task, timeline=timeline)
                if current_task_slot <= dependant_time_slot: #or current_task_slot + int(current_task.duration) < dependant_time_slot + int(dependant_task.duration):
                    cost += DEPENDENCY_VIOLATION_COST
    return cost


def calculate_duration_violation_cost(timeline):
    """
    Calculates the duration cost of the timeline. Any gaps where there is no task running, or there is space between
    slots that are not used is penalised.

    @param timeline: the Timeline instance
    @return: a duration cost factor of the timeline
    """
    times = sorted(timeline.slots.keys())
    cost = 0
    for count, time in enumerate(times):
        assignments = timeline.slots[time]
        max_time = time
        for assignment in assignments:
            if time + int(assignment.task.duration) > max_time:
                max_time = time + int(assignment.task.duration)
        next_slot_time = 0
        if count+1 < len(timeline.slots):
            next_slot_time = times[count+1]
        else:
            break
        if max_time < next_slot_time:
            cost += DURATION_OVERFLOW_COST * (next_slot_time - max_time)
        elif max_time > next_slot_time:
            cost += DURATION_OVERFLOW_COST * (max_time - next_slot_time)
        else:
            cost += DURATION_OVERFLOW_COST * 1
    return cost


def calculate_developer_availablilty_cost(timeline=Timeline):
    """
    Calculates the cost of the timeline in terms of developer availability. Each developer should have a timeline which
    contains their availability for the timeline.
    @return:
    """
    times = sorted(timeline.slots.keys())
    vistited_developers = []
    cost = 0
    for time in times:
        assignments = timeline.slots[time]
        for assignment in assignments:
            developer = assignment.developer
            if developer in vistited_developers:
                continue
            else:
                vistited_developers.append(developer)
            availability = developer.availability
            availability_times = sorted(availability.keys())[time-1:]
            for avail_time in availability_times:
                tasks = availability[avail_time]
                if len(tasks) > 1:
                    cost += len(tasks)-1
    return cost

def get_parent_tasks(task, dependency_graph):
    """
    Recursively returns all the dependent tasks for the specified task. May include duplicates
    @param task: the task to get the predecessors of
    @param dependency_graph: the dependency reference information
    @return: a list of all parent tasks for the specified task
    """
    parents = dependency_graph.predecessors(task)
    for parent in parents:
        parents = parents + get_parent_tasks(parent, dependency_graph)
    return parents


def get_task_time_slot(task=Task, timeline=Timeline):
    """
    Gets the current time slot for the task in the timeline
    @param task: Task
    @param timeline: Timeline
    @return: the slot time where the task in places in the timeline, None if not found
    """
    for slot in timeline.slots:
        for assignment in timeline.slots[slot]:
            if task == assignment.task:
                return slot
    return None
