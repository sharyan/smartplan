import logging
import os
from Bootstrap import load_project
from Scheduler.Developer import DeveloperManager
from Scheduler.Fitness import calculate_dependency_violation, calculate_developer_availablilty_cost, calculate_duration_violation_cost
from Scheduler.Task import TaskManager
from TimelineModel import Population

__author__ = 'Shane Ryan'

"""
    Modelling Assumptions
    Project
        - Developer Pool
            - Headcount
            - Profiles
        - Project Backlog Task Definitions
        - Team Model
        - Agile/Scrum Model
    Task
        - Task Dependency Model
            - Dependency Graph
            - Transitive Dependency
            - Multiple Dependency Ancestory
            - TreeWalker -> calculate score through walking through graph to calculate duration
                - Find critical path, gives minimum length of sprint
                    - If critical path less than sprint length, then critical path has slack
                    - Else, notify that backlog is too big for sprint unless overtime is used
        - Task Estimation Model
            - Task skill requirements
                - Requires some analysis done on the tasks
        - Task Priority Model/Importance Policy
            - Add a priority to the task, based on level of importance
            - If task lies on critical path then is automatically set to a higher priority
    Developer/Worker
        - Consultant vs Payroll employee
        - Timeline Allocation Model/Availability Model
            - Daily slots
            - PTO
            - Task priority rescheduling
            - Fluff tasks, meetings, training days, work events
        - Learning Model/ Training Model
            - Decay model for skills
            - Logarithmic growth in skill level
            - Historic Task Analysis (Evidence based skill aquisition)
            - Training/Brown Bags
                - How to represent an increase in skill
                - Training may help a set of skills, not just one
                - May have tests, evaluations that guarantee that a better skill level was aquired
        - Employee Skill/Experience Model
            - Skill set model
            - Dreyfus model of skill acquisition
            - Skill relationship model
                - Skills are linked in terms of relatedness
                - How to relate skills?
                    - If skills in tasks constantly occur together
                    - If manually specified by the user
                - Skill aquisition theory
    Scheduler
        - Employee-Task Allocation Model/Scheme
            - If task skills and developer skills + levels align well
            -
        - Slot Allocation { Start Date, Task, Developer(s)}
    Team
        - Team Constraint Model
        - Product component ownership
        - Scheduler Resolution (Team to Developer)
    Constraint Model
        - python-constraint, pyconstraint, liglab-constraint
        - Schedule Constraints
        - Development Constraints
        - Time Constraints
        - Quality Constraints
        - Cost Constraints
"""

THIS_ALGORITHM_BECOMING_SKYNET_COST = 999999999

CURRENT_PATH = os.path.realpath(os.path.dirname(__file__))
# create logger
logging.config.fileConfig('../logging.conf')
logger = logging.getLogger('logger_root')

class Solver:

    def __init__(self):
        pass

    def solve(self, task_list, developer_list):
        raise NotImplementedError("Calling a non-implemented version of the solver")


class BasicSolverImpl(Solver):

    def __init__(self, task_manager=TaskManager, developer_manager=DeveloperManager):
        logger.info("Instantiating Solver")
        self.population = Population(100)
        self.task_manager = task_manager
        self.dev_manager = developer_manager

    def seed_population(self):
        self.population.seed_population(task_manager=self.task_manager, developer_manager=self.dev_manager)

    def solve(self):
        # implement the Genetic algorithm for scheduling
        pass

    def next_population(self, previous_population):
        # selection from previous population (Ranked List, Elitism)

        # genetic algorithm operators (crossover, mutation)
        pass

    def ranked__selection(self):
        """
        Selects a individual from the population using the rank selection approach proposed by Baker.
        The proceedure is:
            - Each individual is ranked in increasing order of fitness
            - Using a max value (expected value) of 1.1 (provided by Baker as a reccomendation)
        @return:
        """
        fitnesses = {}
        for individual in self.population.solutions:
            if individual.fitness in fitnesses:
                fitnesses[individual.fitness].append(individual)
            else:
                fitnesses[individual.fitness] = [individual, ]
        ranked_fitness = sorted(fitnesses.keys())
        print ranked_fitness

    def calculate_population_fitness(self):
        """
        Calculates the fitness for a population.

        Uses a penalty system where if a time line breaches a constraint it will gain a penalty score.
        Such penalties are
            - If a developer is overloaded on a particular slot on their timeline
            - If there are gaps between tasks that are not needed (such that the timeline still follows the dependency checks)
            - If the timeline violates the dependency constraints on the tasks

        @param timeline
        @return: the fitness score for the entire population
        """
        total_fitness = 0
        for individual in self.population.solutions:
            # calculate fitness for that individual
            fitness = calculate_dependency_violation(individual, self.task_manager.get_dependency_graph())
            fitness += calculate_developer_availablilty_cost(individual)
            fitness += calculate_duration_violation_cost(individual)
            individual.fitness = fitness
            total_fitness += fitness
            logger.debug("Fitness for individual %s of Solution %s is = %i" % (individual, self.population, fitness))
        return total_fitness

    def __str__(self):
        return str(self.population)

    def __repr__(self):
        return self.__str__()


def main():
    logger.debug("In main")
    task_manager, developer_manager = load_project("\\project.xml")
    graph = task_manager.dependency_graph

    # load data into the solver
    solver = BasicSolverImpl(task_manager=task_manager, developer_manager=developer_manager)
    solver.seed_population()
    total_fitness = solver.calculate_population_fitness()
    print "Total fitness = " + str(total_fitness)
    logger.info("Fitness for population is %i" % total_fitness)

if __name__ == '__main__':
    main()








