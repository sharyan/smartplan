from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the main:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SmartPlan.views.home', name='home'),
    # url(r'^SmartPlan/', include('SmartPlan.foo.urls')),
    # Uncomment the main/doc line below to enable main documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the main:
    url(r'^admin/', include(admin.site.urls)),

    # Below are the url mappings for the SmartPlan Applications Task, Team, Developer, Project, Settings
    # -----------------------------------

    # include the develop application url mappings
    #url(r'^developer/', include('Developer.urls')),

    # include the task application url mappings
    #url(r'^task/', include('Task.urls')),

    # include the team application url mappings
    #url(r'^team/', include('Team.urls')),

    # include the Project application url mappings
    #url(r'^project/', include('Project.urls')),

    #include the Setting application url mappings
    #url(r'settings', include('Settings.urls')),

    # End url mappings for the SmartPlan application
    # -----------------------------------

    # url mapping to the learn side of the application which is used while reading the django tutorial/books
    url(r'^learn/', include('learning.urls')),



)
