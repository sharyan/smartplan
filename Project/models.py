from django.db import models

from Team.models import Team

# TODO add admin classes

class Project(models.Model):
    # TODO add more appropriate fields
    name = models.CharField(max_length=40, blank=False, name='project_name')
    start_date = models.DateTimeField(verbose_name='Start date of the project')

class Component(models.Model):
    # TODO add attributes of a Component
    name = models.CharField(max_length=40, blank=False, name='component_name')
    project = models.ForeignKey(Project)
    team = models.ForeignKey(Team)
