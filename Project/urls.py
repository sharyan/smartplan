__author__ = 'Shane'

from django.conf.urls import patterns, url
from views import create_project, manage_project, manage_tasks, project_home

urlpatterns = \
    patterns('',
             # TODO add url parameters for view logic
             url(r'^$', project_home),
             url(r'^create/$', create_project),
             url(r'^manage/$', manage_project),
             url(r'^tasks/$', manage_tasks),
)
