from django.db import models

from Project.models import Project

# Create your models here.

class Team(models.Model):
    # TODO revise team model structure (many to many relationships between project and teams?, multiple component ownership)
    team_name = models.CharField(max_length=40, blank=False)
    project = models.ForeignKey(Project)
