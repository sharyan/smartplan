__author__ = 'Shane'

from django.conf.urls import patterns, url
from views import team_home, create_team, manage_team

urlpatterns = \
    patterns('',
             url(r'^$', team_home),
             url(r'^create/$', create_team),
             url(r'^manage/$', manage_team),
    )
